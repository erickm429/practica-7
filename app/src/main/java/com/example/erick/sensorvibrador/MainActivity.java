package com.example.erick.sensorvibrador;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button bton_vibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Vibrator vibrador = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        bton_vibrar = (Button)findViewById(R.id.btnvibrar);
        bton_vibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrador.vibrate(600);
            }
        });
    }
}
